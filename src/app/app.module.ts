import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CalendarComponent } from './calendar/calendar.component';
import { MonthViewComponent } from './calendar/components/month-view/month-view.component';
import { WeekViewComponent } from './calendar/components/week-view/week-view.component';
import { CalendarDayComponent } from './calendar/components/calendar-day/calendar-day.component';
import { CalendarBarComponent } from './calendar/components/calendar-bar/calendar-bar.component';
import { CalendarEventComponent } from './calendar/components/calendar-day/calendar-event/calendar-event.component';
import { CalendarEventModalComponent } from './calendar/components/calendar-event-modal/calendar-event-modal.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    MonthViewComponent,
    WeekViewComponent,
    CalendarDayComponent,
    CalendarBarComponent,
    CalendarEventComponent,
    CalendarEventModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
