import {Moment} from 'moment';
import {Validators} from "@angular/forms";

export const daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
export const dateFormControlConfig: [string, Validators] = ['', [Validators.required]];

export enum Duration {
  Hour = 'hour',
  Minute = 'minute',
  Day = 'day',
  Week = 'week',
  Month = 'month',
  Year =  'year'
}

export interface IDayDetails {
  dayDate: Moment,
  events?: ICalendarEvent[];
}

export interface ICalendarEvent{
  id?: string,
  name: string,
  period: Periods,
  isHoliday: boolean,
  dates?: IDatesRange[]
}

export interface IDatesRange{
  startDate: string;
  endDate: string;
}

export interface IModalConfig {
  dayDate?: Moment,
  event?: ICalendarEvent,
  isAddingEvent?: boolean
}

export enum Periods {
  Once = 'once',
  Daily = 'daily',
  Weekly = 'weekly'
}
