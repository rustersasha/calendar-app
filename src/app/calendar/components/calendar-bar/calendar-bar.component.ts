import {ChangeDetectionStrategy, Component, Input, Output, EventEmitter} from '@angular/core';
import {Moment} from 'moment';

@Component({
  selector: 'app-calendar-bar',
  templateUrl: './calendar-bar.component.html',
  styleUrls: ['./calendar-bar.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarBarComponent {
  @Input() set userInterfaceDate(momentDate: Moment) {
    this.month = momentDate.format('MMMM');
    this.year = momentDate.format('yyyy');
  }
  @Input() isWeekView?: boolean;

  @Output() isWeekViewChange = new EventEmitter<boolean>();
  @Output() changeUserInterfaceDate = new EventEmitter<number>();

  month?: string;
  year?: string;

  changeWeekView(state: boolean) {
    this.isWeekViewChange.emit(state);
  }

  changeDate(direction: number) {
    this.changeUserInterfaceDate.emit(direction);
  }
}
