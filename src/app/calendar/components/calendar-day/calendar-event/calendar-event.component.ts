import {ChangeDetectionStrategy, Component, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {Duration, ICalendarEvent, IDatesRange} from "../../../calendar.model";
import {Moment} from 'moment';
import * as moment from 'moment';

@Component({
  selector: 'app-calendar-event',
  templateUrl: './calendar-event.component.html',
  styleUrls: ['./calendar-event.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarEventComponent implements OnChanges{
  @Input() eventDetails?: ICalendarEvent;
  @Input() dayDate?: Moment;

  @Output() deleteEvent = new EventEmitter<string>();
  @Output() editEvent = new EventEmitter<ICalendarEvent>();

  name?: string;
  id?: string;
  range?: IDatesRange;
  duration?: string;
  type?: string;

  ngOnChanges(): void {
    this.name = this.eventDetails?.name;
    this.id = this.eventDetails?.id
    this.type = this.eventDetails?.isHoliday ? 'Holiday' : 'Work Day'
    this.range = this.eventDetails?.dates?.find(({startDate}: IDatesRange) => this.dayDate?.isSame(moment(startDate), Duration.Day))
    this.duration = `${moment(this.range?.startDate).format('HH:mm')} - ${moment(this.range?.endDate).format('HH:mm')}`
  }

  handleDeleteEvent() {
    this.deleteEvent.emit(this.id);
  }

  handleEditEvent() {
    this.editEvent.emit({...this.eventDetails, dates: [{...this.range}]} as ICalendarEvent)
  }
}
