import {ChangeDetectionStrategy, Component, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import * as moment from 'moment';
import {Duration, ICalendarEvent, IDayDetails, IModalConfig} from "../../calendar.model";
import {Moment} from 'moment';

@Component({
  selector: 'app-calendar-day',
  templateUrl: './calendar-day.component.html',
  styleUrls: ['./calendar-day.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarDayComponent implements OnChanges {
  @Input() dayDetails?: IDayDetails;

  @Output() addEvent = new EventEmitter<IModalConfig>();
  @Output() editEvent = new EventEmitter<IModalConfig>();
  @Output() deleteEvent = new EventEmitter<string>();


  month?: string;
  isToday: boolean = false

  ngOnChanges(): void {
    if (this.dayDetails) {
      const lastDayOfMonth = this.dayDetails.dayDate.clone().endOf('month');
      const firstDayOfMonth = this.dayDetails.dayDate.clone().startOf('month');
      const isLastDayOfMonth = this.dayDetails.dayDate.isSame(lastDayOfMonth, 'day');
      const isFirstDayOfMonth = this.dayDetails.dayDate.isSame(firstDayOfMonth, 'day');
      if(isLastDayOfMonth || isFirstDayOfMonth) {
        this.month = this.dayDetails.dayDate.clone().format('MMMM')
      }
      const today = moment().startOf('day');
      this.isToday = this.dayDetails.dayDate.isSame(today, Duration.Day)
    }
  }

  handleAddEvent() {
    this.addEvent.emit({
      dayDate: (this.dayDetails?.dayDate as Moment),
      isAddingEvent: true
    })
  }

  handleDeleteEvent(id: string) {
    this.deleteEvent.emit(id);
  }

  handleEditEvent(event: ICalendarEvent) {
    this.editEvent.emit({
      event,
      dayDate: this.dayDetails?.dayDate,
      isAddingEvent: false
    })
  }

}
