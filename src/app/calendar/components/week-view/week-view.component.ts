import {ChangeDetectionStrategy, Component, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {Moment} from 'moment';
import * as moment from "moment";
import {Duration, ICalendarEvent, IDatesRange, IDayDetails, IModalConfig} from "../../calendar.model";

@Component({
  selector: 'app-week-view',
  templateUrl: './week-view.component.html',
  styleUrls: ['./week-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeekViewComponent implements OnChanges {
  @Input() userInterfaceDate?: Moment;
  @Input() allEvents: ICalendarEvent[] = [];

  @Output() addEvent = new EventEmitter<IModalConfig>()
  @Output() editEvent = new EventEmitter<IModalConfig>()
  @Output() deleteEvent = new EventEmitter<string>();

  week: IDayDetails[] = [];

  ngOnChanges(): void {
    if (this.userInterfaceDate) {
      const weekStart = this.userInterfaceDate.startOf('isoWeek');
      this.week = Array.from(
        {length: 7},
        ((item: undefined, index: number): IDayDetails => {
            const dayDate = moment(weekStart).add(index, 'day').startOf(Duration.Day);
            const events = this.allEvents.filter(({dates}: ICalendarEvent) => {
              return dates?.some(({startDate, endDate}: IDatesRange) => {
                return dayDate.isBetween(startDate, endDate,Duration.Day, '[]');
              })

            });
            return {
              dayDate,
              events
            }
          }
        ))
    }
  }

  handleAddEvent(data: IModalConfig) {
    this.addEvent.emit(data)
  }

  handleEditEvent(data: IModalConfig) {
    this.addEvent.emit(data)
  }

  handleDeleteEvent(id: string) {
    this.deleteEvent.emit(id);
  }
}
