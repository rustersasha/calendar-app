import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {
  dateFormControlConfig,
  Duration,
  ICalendarEvent,
  IDatesRange,
  IModalConfig,
  Periods
} from "../../calendar.model";
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import * as moment from 'moment';
import {Moment} from 'moment';
import {takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";
import {CalendarService} from "../../services/calendar.service";

@Component({
  selector: 'app-calendar-event-modal',
  templateUrl: './calendar-event-modal.component.html',
  styleUrls: ['./calendar-event-modal.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarEventModalComponent implements OnInit, OnDestroy {
  @Input() set config(config: IModalConfig) {
    this.event = config.event;
    this.isAddingMode = config.isAddingEvent;
    this.isHoliday = config.event?.isHoliday as boolean;
    const data = config.isAddingEvent
      ? {
        startDateTime: moment().format('HH:00'),
        endDateTime: moment().add(1, 'hour').format('HH:00'),
        period: Periods.Once
      }
      : {
        name: config.event?.name,
        startDateTime: moment((config.event?.dates as IDatesRange[])[0].startDate).format('HH:00'),
        endDateTime: moment((config.event?.dates as IDatesRange[])[0].endDate).format('HH:00'),
        period: config.event?.period
      };
    this.modalForm.patchValue({
      ...data,
      date: config?.dayDate?.format('YYYY-MM-DD')
    })
  }

  @Output() closeModal = new EventEmitter();
  @Output() addEvent = new EventEmitter<ICalendarEvent>();
  @Output() editEvent = new EventEmitter<ICalendarEvent>();

  destroy$: Subject<void> = new Subject<void>();

  event?: ICalendarEvent
  isHoliday: boolean = false;
  isAddingMode?: boolean;
  Periods = Periods;

  constructor(private formBuilder: FormBuilder,
              private calendarService: CalendarService) {
  }

  modalForm = this.formBuilder.group({
    name: new FormControl('', [Validators.required, Validators.minLength(3),
      Validators.maxLength(10)]),
    period: new FormControl(...dateFormControlConfig),
    startDateTime: new FormControl(...dateFormControlConfig),
    endDateTime: new FormControl(...dateFormControlConfig),
    date: new FormControl(...dateFormControlConfig),
  });

  ngOnInit() {
    this.modalForm.controls["startDateTime"].valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (moment(value, 'hh:mm').isAfter(moment(this.modalForm.value.endDateTime, 'hh:mm'))) {
          this.modalForm.controls["endDateTime"].patchValue(value);
        }
      })
    this.modalForm.controls["endDateTime"].valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (moment(value, 'hh:mm').isBefore(moment(this.modalForm.value.startDateTime, 'hh:mm'))) {
          this.modalForm.controls["startDateTime"].patchValue(value);
        }
      })
  }

  handleCloseModal() {
    this.closeModal.emit();
  }

  onSubmit() {
    let periodDates: IDatesRange[] = [];
    let event: ICalendarEvent = {
      name: this.modalForm.value.name as string,
      period: this.modalForm.value.period as Periods,
      isHoliday: !!this.isHoliday //recreating boolean
    }

    const startDate = moment(this.modalForm.value.date + ' ' + this.modalForm.value.startDateTime)
    const endDate = moment(this.modalForm.value.date + ' ' + this.modalForm.value.endDateTime)
    const firstDateRange = {
      startDate: startDate.toISOString(),
      endDate: endDate.toISOString()
    }

    if (!this.isAddingMode) {
      this.editEvent.emit({
        ...event,
        dates: [{...firstDateRange}],
        id: this.event?.id
      })
      return;
    }

    switch (this.modalForm.value.period) {
      case Periods.Daily:
        periodDates = [...this.generatePeriodDates(Duration.Day, 30, startDate, endDate)]
        break;

      case Periods.Weekly:
        periodDates = [...this.generatePeriodDates(Duration.Week, 3, startDate, endDate)];
        break;
    }

    const dates: IDatesRange[] = [{...firstDateRange}, ...periodDates]

    this.addEvent.emit({
      ...event,
      dates
    });

  }

  private generatePeriodDates(duration: Duration, amount: number, startDate: Moment, endDate: Moment): IDatesRange[] {
    return Array.from({length: amount},
      (_, index) => {
        return {
          startDate: this.calendarService.createDateByIndex(startDate, index, duration).toISOString(),
          endDate: this.calendarService.createDateByIndex(endDate, index, duration).toISOString()
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}


