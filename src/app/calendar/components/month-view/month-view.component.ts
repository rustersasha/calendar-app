import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {Moment} from 'moment';
import {Duration, IDayDetails, IModalConfig} from "../../calendar.model";
import {CalendarService} from "../../services/calendar.service";

@Component({
  selector: 'app-month-view',
  templateUrl: './month-view.component.html',
  styleUrls: ['./month-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MonthViewComponent implements OnChanges{
  @Input() userInterfaceDate!: Moment;
  @Input() allEvents: any[] = [];

  @Output() addEvent = new EventEmitter<IModalConfig>()
  @Output() editEvent = new EventEmitter<IModalConfig>()
  @Output() deleteEvent = new EventEmitter<string>();

  weeks: IDayDetails[][] = [];

  constructor(private calendarService: CalendarService) {
  }

  ngOnChanges(): void {
    if(this.userInterfaceDate) {
      const startEdgeOfCalendar = this.userInterfaceDate.clone()
        .subtract(1, Duration.Month).endOf(Duration.Month).startOf('isoWeek').subtract(1, Duration.Day);

      const arr = Array.from(
        {length: 42},
        ((item, index: number) => {
          const dayDate = this.calendarService.createDateByIndex(startEdgeOfCalendar, index, Duration.Day).startOf(Duration.Day);
          const events = this.allEvents.filter(({dates}) => {
            return dates.some(({startDate, endDate}: any) => {
              return dayDate.isBetween(startDate, endDate,Duration.Day, '[]');
            })

            });
          return {
            dayDate,
            events
          }
          }
        ));

      this.weeks = Array.from({ length: 6 }, () => arr.splice(0, 7));
    }
  }

  handleAddEvent(data: IModalConfig) {
    this.addEvent.emit(data)
  }

  handleEditEvent(data: IModalConfig) {
    this.addEvent.emit(data)
  }

  handleDeleteEvent(id: string) {
    this.deleteEvent.emit(id);
  }
}
