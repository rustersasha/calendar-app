import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {daysOfWeek, Duration, ICalendarEvent, IDatesRange, IModalConfig} from "./calendar.model";
import * as moment from 'moment';
import {Moment} from 'moment';
import {LocalStorageService} from "./services/local-storage.service";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarComponent implements OnInit{
  daysOfWeek: string[] = daysOfWeek;
  userInterfaceMomentDate: Moment = moment()
  allEvents: ICalendarEvent[] = this.localStorageService.getEvents();

  isWeekView: boolean = true;
  modalConfig: IModalConfig | null = null;

  constructor(private localStorageService: LocalStorageService) {
  }

  changeDate(amount: number) {
    const duration = this.isWeekView ? Duration.Week : Duration.Month
    this.userInterfaceMomentDate = this.userInterfaceMomentDate.clone().add(amount, duration)
  }

  ngOnInit(): void {
  }

  openModal(config: IModalConfig) {
    this.modalConfig = config;
  }

  closeModal() {
    this.modalConfig = null;
  }

  addEvent(data: ICalendarEvent) {
    const event = {
      id: this.generateUniqueId(),
      ...data
    }
    this.allEvents = [...this.allEvents, event]
    this.closeModal();
    this.updateStore();
  }

  editEvent(data: ICalendarEvent) {
    const event: ICalendarEvent = this.allEvents.find(item => item.id === data.id) as ICalendarEvent;

    const newStartHours = moment((data.dates as IDatesRange[])[0].startDate).get(Duration.Hour);
    const newEndHours = moment((data.dates as IDatesRange[])[0].endDate).get(Duration.Hour);
    const newStartMinutes = moment((data.dates as IDatesRange[])[0].startDate).get(Duration.Minute);
    const newEndMinutes = moment((data.dates as IDatesRange[])[0].endDate).get(Duration.Minute);
    const dates = event.dates?.map((range:IDatesRange): IDatesRange => {
      return {
        startDate: moment(range.startDate).set({hour: newStartHours, minute: newStartMinutes}).toISOString() as string,
        endDate: moment(range.endDate).set({hour: newEndHours, minute: newEndMinutes}).toISOString() as string
      } as IDatesRange;
    });
    const newEvent = {
      ...data,
      dates
    }
    this.allEvents = [...this.allEvents.filter((event: ICalendarEvent) => event.id !== data.id), newEvent];
    this.closeModal();
    this.updateStore();
  }

  deleteEvent(id: string) {
    this.allEvents = this.allEvents.filter((event: ICalendarEvent) => event.id !== id)
    this.updateStore();
  }

  private generateUniqueId(): string {
    return (new Date()).getTime().toString(36) + Math.random().toString(36).slice(2)
  }

  private updateStore() {
    this.localStorageService.setEvents(this.allEvents);
  }
}


