import {Injectable} from '@angular/core';
import * as moment from "moment";
import {Moment} from "moment";
import {Duration} from "../calendar.model";

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  createDateByIndex(date: Moment, index: number, duration: Duration) {
    return date.clone().add(index + 1, duration)
  }
}
