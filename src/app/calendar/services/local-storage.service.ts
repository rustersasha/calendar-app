import { Injectable } from '@angular/core';
import {ICalendarEvent} from "../calendar.model";

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  getEvents() {
      return JSON.parse(localStorage.getItem('events') as string) || [];
  }

  setEvents(events: ICalendarEvent[]) {
    localStorage.setItem('events', JSON.stringify(events))
  }
}
